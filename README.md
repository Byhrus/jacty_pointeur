```
Souvent, le plus difficile est de savoir "dois-je utiliser un pointeur ou une référence ..." .
Si tu réussies à comprendre cela mon amour, tu ne devrais plus jamais avoir de problème avec les pointeurs.

Avant de commecner,voici un petit tableau récapitulatif de la différence entre les deux
 
                    Pointeurs                            Réréfrences
+-----------------------------------------------------+----------------------------------------+
| Définition       | Le pointeur est l'adresse        | la référence est un alias pour une     |
|                  | mémoire d'une variable           | variable                               |
|------------------+----------------------------------+----------------------------------------+
| Opérateurs       | *  ou ->                         | &                                      |
|------------------+----------------------------------+----------------------------------------+
| Retour           | Un pointeur renvoie la valeur    | Une référence renvoie l'adresse de la  |
|                  | située à l'adresse stockée dans  | variable précédée du signe de référence|
|                  | une variable de pointeur qui est | < & >                                  |
|                  | précédée du signe < * >          |                                        |
|------------------+----------------------------------+----------------------------------------+
| Initialisation   | Aucune initialisation requise    | On peut pas créer une référence sans   |
|                  |                                  | initialisation                         |
|------------------+----------------------------------+----------------------------------------+
| Nulle            | Un pointeur peur faire référence | Une référence ne peut jamais faire     |
|                  | à NULL                           | référence à NULL                       |
|------------------+----------------------------------+----------------------------------------+
| Le moment        | Un pointeur peut être initialiser| Une référence ne peut être initialisée |
| d'initialisation | à tout moment dans le porgramme  | qu'au moment de sa création            |
+-----------------------------------------------------+----------------------------------------+

```

